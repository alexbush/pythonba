import sys

if len(sys.argv) > 1:
    print('Args: ' + ' '.join([arg for arg in sys.argv[1:]]))
else:
    print('No args passed')
