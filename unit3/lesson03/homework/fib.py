def fib(n):
    a, b = 1, 1
    l = []

    for _ in range(n):
        l.append(a)
        a, b = b, a + b

    return l
