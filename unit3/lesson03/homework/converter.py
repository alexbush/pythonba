def dec_to_bin(num, bin_list=[]):

    bin_list = list(bin_list)
    bin_list.insert(0, str(num % 2))
    num = num // 2

    while num > 0:
        return dec_to_bin(num, bin_list)

    return ''.join(bin_list)
