import math

G = 9.81
angle = 45
velocity = 30
rad_angle = angle * math.pi / 180
distance = velocity ** 2 * math.sin(rad_angle * 2) / G
coords = []

# generate chart
chart = ('|' + ' ' * 9) * 10
chart = [chart] * 4
chart = chart + ['-' * 100]
chart = chart * 5

# calculate y per x
for x in range(int(distance)):
    y = math.tan(rad_angle) * x - G / (2 * velocity **
                                       2 * math.cos(rad_angle) ** 2) * x ** 2
    coords.append((x, int(y)))

# add trajectory to chart
for x, y in coords:
    if x < 100 and y < 25:
        row = chart[-y - 1]
        chart[-y - 1] = row[:x] + '*' + row[x + 1:]

# render chart
for i, val in enumerate(chart):
    print(val)

# print x scale
x_values = ''
for i in range(100):
    if i % 11 == 0:
        continue
    if i % 10 == 0:
        x_values += str(i)
    else:
        x_values += ' '
print(x_values)
