def generator(start, end):
    for i in range(start, end):
        yield i**2


for i in generator(1, 10):
    print(i)
