def map(list, function):
    return [function(i) for i in list]


def filter(list, function):
    return [i for i in list if function(i)]


def add_two(el):
    return el + 2


def exclude_odd(el):
    return el % 2 == 0


l = [1, 2, 3, 4, 5, 6]

print(map(l, add_two))
print(filter(l, exclude_odd))
