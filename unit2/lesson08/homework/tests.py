import unittest
from car import Car

class CarTest(unittest.TestCase):
    def setUp(self):
        self.mycar = Car((0, 0))

    def test_change_speed(self):
        self.mycar.change_speed(5)
        self.assertEqual(self.mycar.speed, 5)

    def test_up(self):
        self.mycar.up()
        self.assertEqual(self.mycar.position, (0, 1))

    def test_down(self):
        self.mycar.down()
        self.assertEqual(self.mycar.position, (0, -1))

    def test_left(self):
        self.mycar.left()
        self.assertEqual(self.mycar.position, (-1, 0))

    def test_right(self):
        self.mycar.right()
        self.assertEqual(self.mycar.position, (1, 0))


if __name__ == '__main__':
    unittest.main()
