class Car:
    speed = 1
    direction = 'N'
    position = (0, 0)

    def __init__(self, position):
        self.position = position

    def change_speed(self, speed):
        self.speed = speed

    def move(self):
        if self.direction == 'up':
            self.position = (self.position[0], self.position[1] + self.speed)
        elif self.direction == 'down':
            self.position = (self.position[0], self.position[1] - self.speed)
        elif self.direction == 'left':
            self.position = (self.position[0] - self.speed, self.position[1])
        elif self.direction == 'right':
            self.position = (self.position[0]+ self.speed, self.position[1])


    def up(self):
        self.direction = 'up'
        self.move()
    
    def down(self):
        self.direction = 'down'
        self.move()

    def left(self):
        self.direction = 'left'
        self.move()

    def right(self):
        self.direction = 'right'
        self.move()
    
