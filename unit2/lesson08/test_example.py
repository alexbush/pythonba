import unittest
import demo


class NameTestCase(unittest.TestCase):
    def test_first_and_last_names(self):
        full = demo.full_name('carl', 'johnson')
        full2 = demo.full_name('cARL', 'jOHNSON')
        self.assertEqual(full, 'Carl Johnson')
        self.assertEqual(full2, 'Carl Johnson')


class SquaresTestCase(unittest.TestCase):
    def test_squares(self):
        l = [1, 2, 3, 4]
        l2 = demo.squares(l)
        self.assertEqual(l2, [1, 4, 9, 16])


unittest.main()
