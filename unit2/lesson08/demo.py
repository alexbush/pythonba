def full_name(first, last):
    return first.title() + ' ' + last.title()


def squares(l):
    l2 = []
    for num in l:
        l2.append(num**2)
    return l2
