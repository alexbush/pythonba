class Person:
    def __init__(self, firstname, lastname, age):
        self.firstname = firstname
        self.lastname = lastname
        self.age = age


class Student(Person):
    def __init__(self, firstname, lastname, age, course):
        super().__init__(firstname, lastname, age)
        self.course = course


class Teacher(Person):
    def __init__(self, firstname, lastname, age, salary):
        super().__init__(firstname, lastname, age)
        self.salary = salary


john = Student('John', 'Doe', 19, 'course1')
jane = Teacher('Jane', 'Doe', 58, 3500)

print(jane.salary)
