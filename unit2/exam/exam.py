import json
import datetime

with open('transactions.json') as fo:
    try:
        data = json.load(fo)
    except ValueError:
        data = []


def write_data():
    with open('transactions.json', 'w') as fo:
        fo.write(json.dumps(data, indent=4))


active_person = {}


class Person:
    def __init__(self, firstname, lastname, wallets=[]):
        self.firstname = firstname
        self.lastname = lastname
        self.wallets = wallets[:]

    def add_wallet(self, wallet):
        self.wallets.append(wallet.__dict__)
        data[active_person['index']] = self.__dict__

    def show_wallets(self):
        for i, w in enumerate(self.wallets):
            print('#%s: %s. Ballance: %s' % (i, w['name'], w['ballance']))

    def remove_wallet(self, index):
        del self.wallets[index]

    def show_transactions(self):
        for i in self.wallets:
            wallet = Wallet(**i)
            wallet.show_transactions()


class Wallet:
    def __init__(self, name, ballance=0, transactions=[]):
        self.name = name
        self.ballance = ballance
        self.transactions = transactions[:]

    def add_transaction(self, transaction):
        if transaction.operation == 'income':
            self.ballance += int(transaction.amount)
            self.transactions.append(transaction.__dict__)
        elif transaction.operation == 'outcome':
            self.ballance -= int(transaction.amount)
            self.transactions.append(transaction.__dict__)

    def show_transactions(self):
        print('Wallet: ' + self.name)
        print('Ballance: ' + str(self.ballance))
        print('*' * 20)
        for i in range(-1, -10, -1):
            try:
                transaction = self.transactions[i]
                print('Transaction: %s * Amount: %s * Time: %s' %
                      (transaction['operation'],
                       transaction['amount'],
                       transaction['time']))
            except:
                IndexError
        print('*' * 20)


class Transaction:
    def __init__(self, operation, amount):
        self.operation = operation
        self.amount = amount
        self.time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


while True:

    command = input('Input command (type h for help): ').split()

    if command[0] == 'q':
        break

    elif command[0] == 'h':
        print(
            '''
            - `+p John Doe` - додати людину
            - `lp` - вивести список людей з їх порядковими номерами
            - `-p 1` - видалити людину з вказаним номером
            - `sp 1` - вибрати людину з вказаним номером (подальші транзакції будуть записуватися від відповідного імені)
            - `+w Personal` - додати гаманець
            - `lw` - вивести список гаманців з номерами і залишками
            - `-w 1` - видалити вказаний гаманець разом з усіма прив'язаними транзакціями
            - `+i:1 100` - додати `Income` транзакцію з суммою 100 у гаманець 1 і показати залишок
            - `-i:1 100` - додати `Outcome` транзакцію з суммою 100 у гаманець 1 і показати залишок
            - `last:1` - вивести останні 10 транзакцій з сумами і датами по відповідному гаманцю, а також залишок по ньому
            - `last` - аналогічно, але для всіх гаманців один за одним
            - `q` - вихід з програми
            '''
        )
    elif command[0] == '+p':
        person = Person(command[1], command[2])
        data.append(person.__dict__)
        write_data()

    elif command[0] == 'lp':
        for i, p in enumerate(data):
            print('#%s: %s %s' % (i, p['firstname'], p['lastname']))

    elif command[0] == '-p':
        del data[int(command[1])]

    elif command[0] == 'sp':
        index = int(command[1])
        active_person = {
            'index': index,
            'person': Person(**data[index]),
        }

    elif command[0] == '+w':
        wallet = Wallet(command[1])
        active_person['person'].add_wallet(wallet)
        write_data()

    elif command[0] == 'lw':
        active_person['person'].show_wallets()

    elif command[0] == '-w':
        active_person['person'].remove_wallet(int(command[1]))
        write_data()

    elif '+i:' in command[0]:
        wallet_index = int(command[0].split(':')[1])
        wallet = Wallet(**active_person['person'].wallets[wallet_index])
        transaction = Transaction('income', command[1])
        wallet.add_transaction(transaction)
        active_person['person'].wallets[wallet_index] = wallet.__dict__
        write_data()

    elif '-i:' in command[0]:
        wallet_index = int(command[0].split(':')[1])
        wallet = Wallet(**active_person['person'].wallets[wallet_index])
        transaction = Transaction('outcome', command[1])
        wallet.add_transaction(transaction)
        active_person['person'].wallets[wallet_index] = wallet.__dict__
        write_data()

    elif 'last:' in command[0]:
        wallet_index = int(command[0].split(':')[1])
        wallet = Wallet(**active_person['person'].wallets[wallet_index])
        wallet.show_transactions()

    elif command[0] == 'last':
        active_person['person'].show_transactions()
