import unittest
from exam import Person, Wallet, Transaction

class PersonTest(unittest.TestCase):
    def setUp(self):
        self.person = Person('John', 'Doe', [])
        self.wallet = Wallet('Personal', 0, [])
        self.income = Transaction('income', 100)
        self.income.time = '2019-04-01 20:19:41'
        self.outcome = Transaction('outcome', 50)
        self.outcome.time = '2019-04-01 20:19:41'

    def test_person(self):
        self.person.add_wallet(self.wallet)
        self.assertEqual(self.person.wallets, [{
            'name': 'Personal',
            'ballance': 0,
            'transactions': []
        }])

        self.person.remove_wallet(0)
        self.assertEqual(self.person.wallets, [])

    def test_wallet(self):
        self.wallet.add_transaction(self.income)
        self.assertEqual(self.wallet.transactions, [
            {
                'operation': 'income',
                'amount': 100,
                'time': '2019-04-01 20:19:41'
            }
        ])
        self.assertEqual(self.wallet.ballance, 100)

        self.wallet.add_transaction(self.outcome)
        self.assertEqual(self.wallet.transactions, [
            {
                'operation': 'income',
                'amount': 100,
                'time': '2019-04-01 20:19:41'
            },
            {
                'operation': 'outcome',
                'amount': 50,
                'time': '2019-04-01 20:19:41'
            }
        ])
        self.assertEqual(self.wallet.ballance, 50)
        

if __name__ == '__main__':
    unittest.main()