class Person:
    def __init__(self, firstname, lastname, age):
        self.firstname = firstname
        self.lastname = lastname
        self.age = age

    def talk(self):
        print('Hello, my name is %s %s and I\'m %s years old'
              % (self.firstname, self.lastname, self.age))


andy = Person('Andy', 'James', 38)
andy.talk()
