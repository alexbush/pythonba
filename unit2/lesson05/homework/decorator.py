def name_decorator(func):
    print('Name of current function: ' + func.__name__)

    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


@name_decorator
def greeting(name):
    return 'Hello ' + name


greeting('Andy')
