class Person:
    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname

    @property
    def email(self):
        return self.firstname.lower() + '.' + self.lastname.lower() + '@email.com'

    @property
    def fullname(self):
        return self.firstname.title() + ' ' + self.lastname.title()

    @fullname.setter
    def fullname(self, name):
        self.firstname, self.lastname = name.split(' ')


jw = Person('John', 'Wick')
print(jw.email, jw.fullname)
jw.fullname = 'chris coyier'
print(jw.email)
