max_x = 0
max_y = 0
entry = None
finish = None
walls = []

def get_directions(position, history=None):
    history = history or []
    return list(filter(
        lambda p: (0 <= p[0] <= max_x) and 
        (0 <= p[1] <= max_y) and 
        (p not in walls) and
        (p not in history) and
        (p != entry),
        [
        (position[0] + 1, position[1]),
        (position[0], position[1] + 1),
        (position[0] - 1, position[1]),
        (position[0], position[1] - 1),
    ]))

paths = []

def get_path(start, history=None):
    history = history[:] if history else []
    history.append(start)
    if start == finish:
        return [finish]

    directions = get_directions(start, history)
    result = []
    for d in directions:
        path = get_path(d, history)
        if path and finish in path:            
            result.append([start] + path)
    if result:
        return min(result, key=lambda x: len(x))
            
with open('input.txt', 'r') as f:
    input_data = f.read()

for y, line in enumerate(input_data.splitlines()):
    for x, c in enumerate(line.strip()):
        if x > max_x: max_x = x
        if c == '@':
            entry = (x, y)
        elif c == '$':
            finish = (x, y)
        elif c == '#':
            walls.append((x, y))
    if y == 0: max_x = x

max_y = y

path = get_path(entry)


for y, line in enumerate(input_data.splitlines()):
    for x, c in enumerate(line.strip()):
        print('.' if (x, y) in path else c, end='')
    print()