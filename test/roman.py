numbers = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000
}

roman = input('Enter a roman number: ')
result = 0

for i in range(len(roman)):
    if i < (len(roman) - 1) and numbers[roman[i]] < numbers[roman[i + 1]]:
        result -= numbers[roman[i]]
    else:
        result += numbers[roman[i]]

print('Arabian number is %s' % result)

