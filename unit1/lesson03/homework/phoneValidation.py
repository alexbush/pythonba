phoneNumber = input('Input the phone number: ')
isNumberValid = phoneNumber.isdigit()
isLengthValid = len(phoneNumber) == 10

if (isNumberValid and isLengthValid):
    print('Phone number is valid')
else:
    if not isNumberValid:
        print('Phone number should contain only digits')
    if not isLengthValid:
        print('Phone number should be 10 symbols long')
