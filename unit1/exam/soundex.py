import re


def get_soundex_code(word):
    # remove non-letters and transform to lowercase
    word = re.sub("[^A-Za-z]", "", word).lower()
    first_letter = word[0].upper()

    replace_dict = {
        'a': '0', 'e': '0', 'i': '0', 'o': '0', 'u': '0', 'y': '0',
        'b': '1', 'f': '1', 'p': '1', 'v': '1',
        'c': '2', 'g': '2', 'j': '2', 'k': '2', 'q': '2', 's': '2', 'x': '2', 'z': '2',
        'd': '3', 't': '3',
        'l': '4',
        'm': '5', 'n': '5',
        'r': '6'
    }

    # replace letters by values from dictionary
    word = ''.join(
        [replace_dict[i] if i in replace_dict else i for i in word])

    # remove 'h' and 'w' (but not first symbol)
    word = word[0] + re.sub("[hw]", "", word[1:])

    code = word[0]

    # remove adjacent characters
    for l in word:
        if l != code[-1]:
            code += l

    # remove vowels and join reuslt code string
    code = first_letter + re.sub("0", "", code[1:]).ljust(3, '0')[:3]

    return code


regex_pattern = r'\w+'
with open('words.txt') as fo:
    vocabulary_list = re.findall(regex_pattern, fo.read())

vocabulary_dict = {}
for w in vocabulary_list:
    code = get_soundex_code(w)

    if code in vocabulary_dict:
        vocabulary_dict[code].append(w)
    else:
        vocabulary_dict[code] = [w]

input_text = ''
with open('input.txt') as fo:
    input_text = fo.read()

input_list = []
for i in input_text.splitlines():
    input_list.append(re.findall(regex_pattern, i))


for i, l in enumerate(input_list):
    for w in l:
        if w.lower() not in vocabulary_list:
            suggestions_str = ', '.join(vocabulary_dict[get_soundex_code(w)])
            print('Found unknown word "%s" in line %s. Suggestions: %s' %
                  (w, i+1, suggestions_str))
