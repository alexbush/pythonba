a = 3
b = 2

print('%d + %d = %d' % (a, b, a + b))
print('%d - %d = %d' % (a, b, a - b))
print('%d / %d = %.2f' % (a, b, a / b))
print('%d * %d = %d' % (a, b, a * b))
print('%d %% %d = %d' % (a, b, a % b))
print('%d // %d = %d' % (a, b, a // b))
