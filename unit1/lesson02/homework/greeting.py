import datetime

name = 'Alex'
currentDay = datetime.datetime.now().strftime('%A')

print('Good day, ' + name + '! ' + currentDay +
      'is a perfect day to learn some python')
