import json


def get_info():
    info = {}
    info['firstname'] = input('Input your firstname: ')
    info['lastname'] = input('Input your lastname: ')
    info['phone_number'] = input('Input your phone number: ')
    info['address'] = input('Input your address: ')

    return info


def save_to_file(file, data):
    with open(file, 'w') as fo:
        json.dump(data, fo)


def read_from_file(file):
    with open(file) as fo:
        data = json.loads(fo.read())
        for i in data:
            print(i + ': ' + data[i])
