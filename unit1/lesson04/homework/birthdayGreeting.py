userName = input('Input your name: ')
userAge = input('Input your age: ')

while not userAge.isdigit():
    userAge = input('Input your age in number format: ')

print('Hello {}, on your next birthday you’ll be {} years'.format(
    userName, int(userAge) + 1))
