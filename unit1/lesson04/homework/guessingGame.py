import random

randNum = random.randint(1, 10)
userNum = input('Type a number between 1 and 10: ')

while not userNum.isdigit():
    userNum = input('Wrong format! Please, input a valid number: ')

if randNum == userNum:
    print('You\'re lucky!')
else:
    print('You\'ll be lucky next time. Maybe...')
