import operator
from functools import reduce


def make_operation(op, *args):
    operators = {
        '+': operator.add,
        '-': operator.sub,
        '*': operator.mul
    }

    try:
        return reduce(lambda x, y: operators[op](x, y), args)
    except:
        return 'You can only use + - or * and arguments should only be numbers'


print(make_operation('+', 7, 7, 2))
