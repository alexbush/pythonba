from collections import Counter

sentence = 'hello world hello world again'
sentenceList = sentence.split()

# using comprehension
resultDict = {i: sentenceList.count(i) for i in sentenceList}

# using Counter
# resultDict = dict(Counter(sentenceList))

print(resultDict)
