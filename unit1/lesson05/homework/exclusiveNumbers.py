import random

arrA = [random.randint(1, 10) for i in range(10)]
arrB = [random.randint(1, 10) for i in range(10)]

resultArr = list(set(arrA) & set(arrB))

print(arrA, arrB, resultArr, sep='\n')
